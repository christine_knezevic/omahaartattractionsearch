$(document).ready();

function show(id){
	switch(id){
		case 1:
		$("#add").css("display", "inline");
		$("#change").css("display", "none");
		$("#delete").css("display", "none");
		$(".welcome").css("display", "none");
		break;
		
		case 2:
		$("#change").css("display", "inline");
		$("#add").css("display", "none");
		$("#delete").css("display", "none");
		$(".welcome").css("display", "none");
		break;
		
		case 3:
		$("#delete").css("display", "inline");
		$("#add").css("display", "none");
		$("#change").css("display", "none");
		$(".welcome").css("display", "none");
		
		break;
		
		default:
		$("#add").css("display", "none");
		$("#change").css("display", "none");
		$("#delete").css("display", "none");
		$(".welcome").css("display", "none");
		break;
	}
}